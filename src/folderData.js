export const data = [
  {
    title: 'src',
    children: [
      {
        title: 'components',
        children: [
          {title: 'Home', children: []},
          {title: 'About', children: []},
        ],
      },
      {
        title: 'redux',
        children: [
          {title: 'actions', children: []},
          {title: 'reducers', children: []},
        ],
      },
    ],
  },
  {
    title: 'assets',
    children: [
      {title: 'images', children: []},
      {title: 'fonts', children: []},
    ],
  },
];
